# cockroach-core [![pipeline status](https://gitlab.com/cockroach-poker/cockroach-core/badges/master/pipeline.svg)](https://gitlab.com/cockroach-poker/cockroach-core/-/commits/masterst) [![coverage report](https://gitlab.com/cockroach-poker/cockroach-core/badges/master/coverage.svg)](https://gitlab.com/cockroach-poker/cockroach-core/-/commits/master)


Core game logic implemented in a python package.
