#!/bin/bash

echo "coverage..."
coverage run --source=cockroachcore --omit="/*tests*" -m nose
coverage html
coverage report
echo "bandit..."
bandit -r cockroachcore
echo "flake8..."
flake8 setup.py cockroachcore --per-file-ignores="__init__.py:F401"
echo "done."
