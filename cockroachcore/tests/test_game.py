from unittest import TestCase
from cockroachcore import Game, Player, Suit, Action, Turn


class TestGameStart(TestCase):
    def test_create(self):
        game = Game()
        self.assertEqual(0, len(game.players))

    def test_join(self):
        game = Game()
        game.join('bob')
        self.assertEqual(
                {'bob': Player(name='bob')},
                game.players)

    def test_deal(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.join('jan')
        game.join('pam')
        game.deal()
        self.assertEqual(
                1,
                len([p for n, p in game.players.items() if len(p.hand) == 12]))
        self.assertEqual(
                4,
                len([p for n, p in game.players.items() if len(p.hand) == 13]))

    def test_first_turn(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.join('jan')
        game.join('pam')
        game.deal()
        self.assertEqual({Action.PLAY}, game.turn.actions)
        self.assertTrue(game.turn.player in game.players.keys())
        self.assertEqual([], game.turn.played)


class TestGamePlay(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.turn = Turn('bob')

    def test_play(self):
        self.assertEqual('bob', self.game.turn.player)
        self.assertEqual({Action.PLAY}, self.game.turn.actions)
        self.game.play('bill', Suit.FLY, Suit.BAT)
        self.assertEqual([Suit.FLY, Suit.BAT], self.game.players['bob'].hand)
        self.assertEqual('bill', self.game.turn.player)
        self.assertEqual({Action.CALL, Action.PASS}, self.game.turn.actions)
        self.assertEqual(['bob'], self.game.turn.played)
        self.assertEqual('bill', self.game.turn.player)
        self.assertEqual(Suit.FLY, self.game.turn.card)
        self.assertEqual(Suit.BAT, self.game.turn.claim)


class TestGameCallPassLie(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.turn = Turn('bob')
        self.game.play('bill', Suit.FLY, Suit.BAT)

    def test_call_correct(self):
        result = self.game.call(False)
        self.assertTrue(result)
        self.assertEqual('bob', self.game.turn.player)
        self.assertEqual({Action.PLAY}, self.game.turn.actions)
        self.assertEqual([], self.game.turn.played)
        self.assertEqual({Suit.FLY: 1}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    def test_call_incorrect(self):
        result = self.game.call(True)
        self.assertFalse(result)
        self.assertEqual('bill', self.game.turn.player)
        self.assertEqual({Action.PLAY}, self.game.turn.actions)
        self.assertEqual([], self.game.turn.played)
        self.assertEqual({Suit.FLY: 1}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    def test_pass(self):
        self.game.pass_to('sue', Suit.RAT)
        self.assertEqual('sue', self.game.turn.player)
        self.assertEqual({Action.CALL}, self.game.turn.actions)
        self.assertEqual(['bob', 'bill'], self.game.turn.played)
        self.assertEqual(Suit.FLY, self.game.turn.card)
        self.assertEqual(Suit.RAT, self.game.turn.claim)
        self.assertEqual({}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    # TODO: invalid moves
    # TODO: more than 3 players for multiple pass case


class TestGameCallPassTruth(TestCase):
    def setUp(self):
        self.game = Game()
        self.game.join('bob')
        self.game.join('bill')
        self.game.join('sue')
        self.game.players['bob'].hand = [Suit.FLY, Suit.FLY, Suit.BAT]
        self.game.turn = Turn('bob')
        self.game.play('bill', Suit.FLY, Suit.FLY)

    def test_call_correct(self):
        result = self.game.call(True)
        self.assertTrue(result)
        self.assertEqual('bob', self.game.turn.player)
        self.assertEqual({Action.PLAY}, self.game.turn.actions)
        self.assertEqual({Suit.FLY: 1}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    def test_call_incorrect(self):
        result = self.game.call(False)
        self.assertFalse(result)
        self.assertEqual('bill', self.game.turn.player)
        self.assertEqual({Action.PLAY}, self.game.turn.actions)
        self.assertEqual([], self.game.turn.played)
        self.assertEqual({Suit.FLY: 1}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    def test_pass(self):
        self.game.pass_to('sue', Suit.RAT)
        self.assertEqual('sue', self.game.turn.player)
        self.assertEqual({Action.CALL}, self.game.turn.actions)
        self.assertEqual(['bob', 'bill'], self.game.turn.played)
        self.assertEqual(Suit.FLY, self.game.turn.card)
        self.assertEqual(Suit.RAT, self.game.turn.claim)
        self.assertEqual({}, self.game.players['bill'].tabled)
        self.assertEqual({}, self.game.players['bob'].tabled)
        self.assertEqual({}, self.game.players['sue'].tabled)

    # TODO: invalid moves
    # TODO: more than 3 players for multiple pass case


class TestGameCheckLoser(TestCase):
    def test_no_loser(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        self.assertEqual(None, game.check_loser())

    def test_sue_lost(self):
        game = Game()
        game.join('bob')
        game.join('bill')
        game.join('sue')
        game.players['sue'].tabled = {Suit.FLY: 4, Suit.RAT: 3}
        game.players['bill'].tabled = {Suit.FLY: 3, Suit.BAT: 3}
        game.players['bob'].tabled = {Suit.STINKBUG: 1}
        self.assertEqual('sue', game.check_loser())


# TODO: check_loser()
